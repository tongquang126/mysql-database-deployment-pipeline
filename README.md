#About the Project
## The project uses GitLab CI/CD to add new data into remote MySQL database 
* Pipeline flow: GitLab Repo ---[trigger]---> GitLab Runner (on EC2 with Ansible) ---[Deployment] ---> MySQL Server 
* Use GitLab repo to store the user.sql file which adds new data into the MySQL table
* Gitlab Runner run on AWS EC2 with Ansible installation 
* MySQL server run on AWS EC2 with public IP $ANSIBLE_SLAVE_IP. Note that, you must change the Variable $ANSIBLE_SLAVE_IP to match the MySQL Serer IP address 
* GitLab Runner (Ansible) remotely connect to MySQL Server and run the user.sql file using DB_Deployment_Playbook.yml
